/*
 * conf_directC.h
 *
 * Created: 16.10.2013 21:26:18
 *  Author: Yaro
 */ 


#ifndef CONF_DIRECTC_H_
#define CONF_DIRECTC_H_

#define BULK_USB_DATA_SENDING   //Define if Bulk Data sending schould be used (makes things faster)
#define PRECOMPUTE_PROGRAMMING_ON_PC //Define if programming sequence should be precomputed on the PC (makes things faster)








typedef enum programmerCommand {
	Programmer_Command_WAIT_FOR_COMMAND,
	Programmer_Command_SEND_DATA,
	Programmer_Command_PRINT_STRING,
	Programmer_Command_PRINT_STD_MESSAGE,
	
	Programmer_Command_GET_ROW_SIGNALS,
	Programmer_Command_GET_ROW_SIGNALS_COMPRESSED,

	Programmer_Command_FINISH_CURRENT_ACTION
} programmerCommand;



#endif /* CONF_DIRECTC_H_ */