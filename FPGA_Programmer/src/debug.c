/*
 * debug.c
 *
 * Created: 08.05.2014 22:06:59
 *  Author: Yaro
 */ 

#include "debug.h"


void print_debug(const char* format, ...){
	if (USE_DEBUG){
		uint8_t characterCount;
		va_list argptr;
		va_start(argptr, format);
		characterCount = vsnprintf(printDebugBuffer, PRINT_DEBUG_BUF_SIZE, format, argptr);
		va_end(argptr);
		
		udi_cdc_multi_write_buf(USB_PC_INTERACTIONS_PORT, printDebugBuffer, characterCount);
	}
}