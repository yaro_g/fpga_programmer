/*
 * PCInteractions.h
 *
 * Created: 04.11.2013 22:26:57
 *  Author: Yaro
 */ 


#ifndef PCINTERACTIONS_H_
#define PCINTERACTIONS_H_

#include <inttypes.h>
#include "udi_cdc.h"

typedef enum programmerMode {
	Programmer_Mode_FPGA_PROGRAMMER = 0, Programmer_Mode_DATA_LINKER_SPI = 1, Programmer_Mode_DATA_LINKER_UART = 2, Programmer_Mode_INFO = 3
} programmerMode;



typedef enum dataLinkMode {
	Data_Link_Mode_SEND = 0, Data_Link_Mode_SEND_AND_RECEIVE = 1, Data_Link_Mode_RECEIVE = 2
} dataLinkMode;

uint32_t usb_getUint32(uint8_t port);
uint16_t usb_getUint16(uint8_t port);
void usb_putUint32(uint8_t port, uint32_t data);
void usb_putUint16(uint8_t port, uint16_t data);

#endif /* PCINTERACTIONS_H_ */