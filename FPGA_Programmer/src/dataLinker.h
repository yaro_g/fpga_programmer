/*
 * dataLinker.h
 *
 * Created: 06.11.2013 22:01:26
 *  Author: Yaro
 */ 


#ifndef DATALINKER_H_
#define DATALINKER_H_
#include <asf.h>

#define DATA_LINKER_PACKET_SIZE 10000
#define DATA_LINKER_BUF_SIZE 100 // <256
#define USB_DATA_LINKER_PORT 0

uint8_t dataLinkerBuf[DATA_LINKER_BUF_SIZE]; 




#endif /* DATALINKER_H_ */