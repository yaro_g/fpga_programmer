/*
 * uart.h
 *
 * Created: 24.11.2013 21:18:34
 *  Author: Yaro
 */ 


#ifndef UART_H_
#define UART_H_

#include "asf.h"
#include "udi_cdc.h"
#include "dataLinker.h"
#include "PCInteractions.h"

#define RTS_PIN_bm PIN5_bm //changes in init.c have to be done manually
#define CTS_PIN_bm PIN2_bm

void initUART(uint32_t baudrate);
void deInitUART(void);
ISR(USARTC1_RXC_vect);

void handleDataLinkerUART(void);

inline void setRTS(void);
inline void clearRTS(void);
inline uint8_t isCTSSet(void);

inline void setRTS(void){
	PORTC.OUTSET = RTS_PIN_bm;
}

inline void clearRTS(void){
	PORTC.OUTCLR = RTS_PIN_bm;
}

inline uint8_t isCTSSet(void){
	if (PORTC.IN & CTS_PIN_bm){
		return 1;
	}
	else{
		return 0;
	}
}

#endif /* UART_H_ */