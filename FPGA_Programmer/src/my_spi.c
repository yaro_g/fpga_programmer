/*
 * spi.c
 *
 * Created: 08.11.2013 15:30:13
 *  Author: Yaro
 */ 

#include "my_spi.h"

void initSPIMaster(uint8_t prescaler){
	sysclk_enable_peripheral_clock(&SPIC);
	
	PORTC.OUTSET = PIN2_bm; //SS FPGA
	PORTC.OUTCLR = PIN7_bm;
	
	PORTC.DIRSET = PIN7_bm | PIN5_bm | PIN2_bm;
	PORTC.DIRCLR = PIN6_bm;
	
	PORTC.DIRSET = PIN4_bm; //my SS and Clock output
	
	
	switch (prescaler){
	case 2: 
		SPIC.CTRL |= SPI_PRESCALER_DIV4_gc | SPI_CLK2X_bm;
	 	break;
	case 4:
		SPIC.CTRL |= SPI_PRESCALER_DIV4_gc;
		break;
	case 8:
		SPIC.CTRL |= SPI_PRESCALER_DIV16_gc | SPI_CLK2X_bm;
		break;
	case 16:
		SPIC.CTRL |= SPI_PRESCALER_DIV16_gc;
		break;
	case 32:
		SPIC.CTRL |= SPI_PRESCALER_DIV64_gc | SPI_CLK2X_bm;
		break;
	case 64:
		SPIC.CTRL |= SPI_PRESCALER_DIV64_gc;
		break;
	case 128:
		SPIC.CTRL |= SPI_PRESCALER_DIV128_gc;
		break;
	default:
		SPIC.CTRL |= SPI_PRESCALER_DIV128_gc;
	}
	
	SPIC.CTRL |= SPI_ENABLE_bm | SPI_MASTER_bm | SPI_MODE_3_gc;
	
	SPIC.INTCTRL = 0; 		
 }
 
void deInitSPIMaster(void){
	 PORTC.DIRCLR = PIN7_bm | PIN5_bm | PIN2_bm;
	 PORTC.DIRCLR = PIN6_bm;
	 	 
	 SPIC.CTRL = 0;
	 SPIC.INTCTRL = 0;
 }


 
void selectSPISlaveFPGA(void){
	PORTC.OUTCLR = PIN2_bm;
}
 
void deSelectSPISlaveFPGA(void){
	PORTC.OUTSET = PIN2_bm;
	
	//debug!!!!!!!!!!!!!!!!!!!!!!!! for karstens spi ipcore
	deInitSPIMaster();
	PORTC.DIRSET = PIN2_bm | PIN5_bm | PIN7_bm;
	PORTC.OUT = PIN2_bm | PIN5_bm; //SS high and sck 
	PORTC.OUT = PIN2_bm | PIN5_bm | PIN7_bm;
	initSPIMaster(2);
	
	
}
 

void blockingBufferTransferAndReceiveSPI(uint8_t* buffer, uint8_t byteCount){
	while(byteCount>0){
		selectSPISlaveFPGA();
		
		SPIC.DATA = *buffer;
		byteCount--;
		
		while(!SPIC.STATUS);
		SPIC.STATUS = SPI_IF_bm; //delete flag
		
		deSelectSPISlaveFPGA();
		
		*buffer = SPIC.DATA;
		
		buffer++;
	}
}

void blockingBufferTransferSPI(uint8_t* buffer, uint8_t byteCount){
	while(byteCount>0){
		selectSPISlaveFPGA();
		
		SPIC.DATA = *buffer;
		byteCount--;
		
		while(!SPIC.STATUS);
		SPIC.STATUS = SPI_IF_bm; //delete flag
		
		deSelectSPISlaveFPGA();
	
		buffer++;
	}
}

void blockingBufferReceiveSPI(uint8_t* buffer, uint8_t sendChar, uint8_t byteCount){
	while(byteCount>0){
		selectSPISlaveFPGA();
		
		SPIC.DATA = sendChar;
		byteCount--;
		
		while(!SPIC.STATUS);
		SPIC.STATUS = SPI_IF_bm; //delete flag
		
		deSelectSPISlaveFPGA();
				
				
		
		*buffer = SPIC.DATA;
		
		buffer++;
	}
}

uint8_t debugBuf[100];

void handleDataLinkerSPI(void){
	dataLinkMode nextDataLinkMode;
	uint32_t byteCount;
	
	do{
		nextDataLinkMode = udi_cdc_multi_getc(USB_DATA_LINKER_PORT);
		byteCount = usb_getUint32(USB_DATA_LINKER_PORT);
		//usb_putUint32(USB_DATA_LINKER_PORT, byteCount);

		rtc_set_time (0);
		TCC0.CNT = 0;
		
		uint32_t bytesLeft = byteCount;
		
		while (bytesLeft != 0) {
			uint32_t processDataPackageBytes;
			if (bytesLeft <= DATA_LINKER_PACKET_SIZE) {
				processDataPackageBytes = bytesLeft;
				} else {
				processDataPackageBytes = DATA_LINKER_PACKET_SIZE;
			}

			if(!(processDataPackageBytes%64)) { //workaround for bug in usb lib
				processDataPackageBytes--;
			}
			
			
			uint32_t processDataPackageBytesCopy = processDataPackageBytes; //workaround for bug in usb lib
			
			while(processDataPackageBytesCopy!=0) {
				uint32_t processBytes;
				if (processDataPackageBytesCopy <= DATA_LINKER_BUF_SIZE) {
					processBytes = processDataPackageBytesCopy;
					} else {
					processBytes = DATA_LINKER_BUF_SIZE;
				}
				
				
				switch(nextDataLinkMode){
					case Data_Link_Mode_RECEIVE:
					blockingBufferReceiveSPI(dataLinkerBuf, 0, processBytes);
					udi_cdc_multi_write_buf(USB_DATA_LINKER_PORT, dataLinkerBuf, processBytes);
					break;
					case Data_Link_Mode_SEND:
					udi_cdc_multi_read_buf(USB_DATA_LINKER_PORT, dataLinkerBuf, processBytes);
					blockingBufferTransferSPI(dataLinkerBuf, processBytes);
					break;
					case Data_Link_Mode_SEND_AND_RECEIVE:
					udi_cdc_multi_read_buf(USB_DATA_LINKER_PORT, dataLinkerBuf, processBytes);
					blockingBufferTransferAndReceiveSPI(dataLinkerBuf, processBytes);
					udi_cdc_multi_write_buf(USB_DATA_LINKER_PORT, dataLinkerBuf, processBytes);
					break;
				}
				

				processDataPackageBytesCopy -= processBytes;
			}

			bytesLeft -= processDataPackageBytes;
		}

		
		

		//usb_putUint32(USB_DATA_LINKER_PORT, debug);

		//timing information
// 		uint32_t time2 = (uint32_t)TCC0.CNT;
// 		uint32_t time1 = rtc_get_time();
// 		udi_cdc_write_buf(debugBuf, sprintf(debugBuf, "RTC:%lu  Timer%lu", time1,time2)+1);
// 		usb_putUint32(USB_DATA_LINKER_PORT, time1);
		
	}while (byteCount != 0);
}