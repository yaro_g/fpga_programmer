/*
 * PC_Interaction.c
 *
 * Created: 18.10.2013 18:01:31
 *  Author: Yaro
 */ 
#include "DirectC_PC_Interaction.h"


void directCSendCommandToPC(programmerCommand command )
{
	udi_cdc_multi_putc(USB_DIRECTC_PORT, command);
}



void handleProgrammerMode(void){
	Action_code = udi_cdc_multi_getc(USB_DIRECTC_PORT);
	
// 	dp_display_text("\n\nAction code: ");
// 	dp_display_value(Action_code,DEC);
// 	dp_display_text("\n");

	uint8_t errCode = dp_top();
	
	if(udi_cdc_multi_get_nb_received_data(USB_DIRECTC_PORT)){
		dp_display_text("\n\nmain available data!: ");  //debug!!! should be standard message
		dp_display_value(udi_cdc_multi_get_nb_received_data(USB_DIRECTC_PORT),DEC);
		dp_display_text("This is a bug. Please report it.\n");
		// 					for(int i = 0; i < 2398; i++){
		// 						dp_display_text("\n");  //debug
		// 						dp_display_value(debugRowBuffer[i],DEC);
		// 					}
	}
	
	if(errCode != 0){
		dp_display_text("Something went wrong!\n\nerror code: ");  //debug!!! should be standard message
		dp_display_value(errCode,DEC);
		dp_display_text("\nLook it up in directc_ug.pdf, p.38\n");
	}
	
	
	
	directCSendCommandToPC(Programmer_Command_FINISH_CURRENT_ACTION);
}