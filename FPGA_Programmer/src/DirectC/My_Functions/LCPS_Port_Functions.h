/*
 * LCPS_Port_Functions.h
 *
 * Created: 09.10.2013 11:36:14
 *  Author: gevorkov
 */ 


#ifndef LCPS_PORT_FUNCTIONS_H_
#define LCPS_PORT_FUNCTIONS_H_

#include <avr/io.h>
#include "conf_board.h"

void initLCPSPort(void);
void releaseLCPSPort(void);

#endif /* LCPS_PORT_FUNCTIONS_H_ */
