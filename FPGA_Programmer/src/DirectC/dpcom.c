/* ************************************************************************ */
/*                                                                          */
/*  DirectC         Copyright (C) Microsemi Corporation 2013                */
/*  Version 3.0     Release date September 30, 2013                         */
/*                                                                          */
/* ************************************************************************ */
/*                                                                          */
/*  Module:         dpcom.c                                                 */
/*                                                                          */
/*  Description:    Contains functions for data table initialization &      */
/*  pointers passing of the various data blocks needed.                     */
/*                                                                          */
/* ************************************************************************ */
#include "dpuser.h"
#include "dpcom.h"
#include "dpjtag.h"

#include "DirectC_PC_Interaction.h"

/*
* Paging System Specific Implementation.  User attention required:
*/
DPUCHAR *image_buffer; /* Pointer to the image in the system memory */
DPUCHAR *page_buffer_ptr;
#ifdef USE_PAGING
DPUCHAR page_global_buffer[PAGE_BUFFER_SIZE];  /* Page_global_buffer simulating the global_buf1fer that is accessible by DirectC code*/
DPULONG page_address_offset;
DPULONG start_page_address = 0u;
DPULONG end_page_address = 0u;
#endif

DPULONG return_bytes;
DPULONG requested_bytes;
/*
* current_block_address holds the data block starting address in the image file that the page global_buf1fer data is currently holding
*/
DPULONG current_block_address=0U;
DPUCHAR current_var_ID=Header_ID;
DPULONG image_size=MIN_IMAGE_SIZE;

/*
* Module: dp_get_data
* 		purpose: This function is called only when access to the any data block within the image is needed.
*		It is designed to return the pointer address of the first byte containing bit_index in the data block 
*		requested by the var_identifier (var_ID). 
* Arguments: 
*		DPUCHAR * var_ID, an unsigned integer variable contains an identifier specifying which 
* 		block address to return.
*		DPULONG bit_index: The bit location of the first bit to be processed (clocked in) within the data block specified by Var_ID.
* Return value: 
* 		Address point to the first element in the block.
* 
*/
// DPUCHAR* dp_get_data(DPUCHAR var_ID,DPULONG bit_index)
// {
//     DPUCHAR * data_address = (DPUCHAR*)NULL;
//     dp_get_data_block_address(var_ID);
//     if ((current_block_address == 0U) && (var_ID != Header_ID))
//     {
//         return_bytes = 0U;
//     }
//     else
//     {
//         data_address = dp_get_data_block_element_address(bit_index);
//     }
//     return data_address;
// }


DPUCHAR* dp_get_data(DPUCHAR var_ID,DPULONG bit_index)
{
	DPULONG image_requested_address;
	DPULONG page_address_offset;
	
	if (var_ID == Header_ID)
	current_block_address = 0;
	else
	dp_get_data_block_address(var_ID);
	
	if ((current_block_address ==0) && (var_ID != Header_ID))
	{
		return_bytes = 0;
		return NULL;
	}
	/* Calculating the relative address of the data block needed within the image */
	image_requested_address = current_block_address + bit_index / 8;
	/* If the data is within the page, adjust the pointer to point to the particular
	element requested */
	if ((image_requested_address >= start_page_address) && (image_requested_address
	<= end_page_address))
	{
		page_address_offset = image_requested_address - start_page_address;
		return_bytes = end_page_address - image_requested_address + 1;
	}
	/* Otherwise, call dp_get_page_data which would fill the page with a new data
	block */
	else
	{
		dp_get_page_data(image_requested_address);
		page_address_offset = 0;
	}
	return &page_global_buffer[page_address_offset];
}




/*
* Module: dp_get_header_data
* 		purpose: This function is called only when access to the header data block within the image is needed.
*		It is designed to return the pointer address of the first byte containing bit_index in the header block.
* Arguments: 
*		DPULONG bit_index: The bit location of the first bit to be processed (clocked in) within the data block specified by Var_ID.
* Return value: 
* 		Address point to the first element in the block.
* 
*/
DPUCHAR* dp_get_header_data(DPULONG bit_index)
{
    /* The first block in the image is the header.  There is no need to get the address of its block.  It is zero. */
    current_block_address = 0U;
    
    /* Calculating the relative address of the data block needed within the image */
    return dp_get_data_block_element_address(bit_index);
}

/*
* User attention: 
* Module: dp_get_page_data
* 		purpose: This function is called by dp_get_data function.  This is done every time a new data block is needed
*		or when requested data is not in the current page.  This function expects the location of the first byte to return
*		and will fill the entire page with valid data.
* Arguments: 
*		DPULONG image_requested_address, a ulong variable containing the relative address location of the first needed byte 
* Return value:
* 		None.
* 
*/
#ifdef USE_PAGING

#ifdef BULK_USB_DATA_SENDING
// bulk data sending 
uint32_t last_requested_byte_count;
uint32_t last_image_requested_address;


void skipData(uint32_t count){
	uint32_t bufferSkipTimes = count/PAGE_BUFFER_SIZE;
	
	for (uint32_t i = 0; i < bufferSkipTimes; i++ ){
		udi_cdc_read_buf(page_global_buffer, PAGE_BUFFER_SIZE);
	}
	
	for (uint32_t i = bufferSkipTimes*PAGE_BUFFER_SIZE; i < count; i++){
		udi_cdc_getc();
	}
}

void skipAllData(void){
	if(end_page_address != 0){	
// 		dp_display_text("\nskipping all ");
// 		dp_display_value(last_requested_byte_count - end_page_address - 1, DEC);// 		dp_display_text("\n");
	
		skipData(last_requested_byte_count - end_page_address - 1);
		last_requested_byte_count = 0;
		last_image_requested_address = 0;
		end_page_address = 0;
		start_page_address = 0;
	
		///////////	//debug
		delay_ms(10);
// 		if(udi_cdc_multi_get_nb_received_data(0) ){
// 			dp_display_text("\n\navailable data: ");
// 			dp_display_value(udi_cdc_multi_get_nb_received_data(0),DEC);
// 			dp_display_text("\n");
// 		}
	}else{
// 		dp_display_text("\nskipping all again! too much!");
// 		if(udi_cdc_multi_get_nb_received_data(0) ){
// 			dp_display_text("\n\nnot too much, because data available!!!: ");  //debug
// 			dp_display_value(udi_cdc_multi_get_nb_received_data(0),DEC);
// 			dp_display_text("\n");
// 		}

	}
}


void dp_get_page_data(DPULONG image_requested_address)
{
	if(last_requested_byte_count == 0){
		//request data
		directCSendCommandToPC(Programmer_Command_SEND_DATA);

		usb_putUint32(USB_DIRECTC_PORT, image_size);
			
		last_requested_byte_count = image_size;
		
		skipData(image_requested_address);
	}
	else if(image_requested_address < end_page_address + 1){
// 		dp_display_text("\nskipping for getting new one ");
// 		dp_display_value(last_requested_byte_count - end_page_address - 1, DEC);
		
		skipData(last_requested_byte_count - end_page_address - 1);

		//request data
		directCSendCommandToPC(Programmer_Command_SEND_DATA);
		//send count
		usb_putUint32(USB_DIRECTC_PORT, image_size);
		
		last_requested_byte_count = image_size;
		
		skipData(image_requested_address);
	}
	else if(image_requested_address > end_page_address + 1 && image_requested_address < last_requested_byte_count){
// 		dp_display_text("\nskipping ");
// 		dp_display_value(last_requested_byte_count - end_page_address - 1, DEC);
		
		skipData(image_requested_address - end_page_address - 1);
	}
	else if(image_requested_address > end_page_address + 1 && image_requested_address >= last_requested_byte_count){
// 		dp_display_text("\nskipping to end, getting new and skipping again ");
// 		dp_display_value(last_requested_byte_count - end_page_address - 1, DEC);
		
		skipData(last_requested_byte_count - end_page_address - 1);
		//request data
		directCSendCommandToPC(Programmer_Command_SEND_DATA);
		//send count
		usb_putUint32(USB_DIRECTC_PORT, image_size);
		
		last_requested_byte_count = image_size;
		
		skipData(image_requested_address);
	}

	//DPULONG image_address_index;
	//start_page_address=0;

	//image_address_index=image_requested_address;
	return_bytes = PAGE_BUFFER_SIZE;

	if (image_requested_address + return_bytes > image_size)
	return_bytes = image_size - image_requested_address;

	//dp_display_text("\nreading address ");
	//dp_display_value(image_requested_address, DEC);
	
	udi_cdc_multi_read_buf(USB_DIRECTC_PORT, page_global_buffer, return_bytes);
	
	start_page_address = image_requested_address;
	end_page_address = image_requested_address + return_bytes - 1;

	last_image_requested_address = image_requested_address;
	return;
}

#else
// // Sending Data on Demand
void dp_get_page_data(DPULONG image_requested_address)
{
	return_bytes = PAGE_BUFFER_SIZE;
	
	if (image_requested_address + return_bytes > image_size)
	return_bytes = image_size - image_requested_address;

	
	directCSendCommandToPC(Programmer_Command_SEND_DATA);
	//send address
	usb_putUint32(USB_DIRECTC_PORT, image_requested_address);

	//send byte count
	udi_cdc_multi_putc(USB_DIRECTC_PORT, return_bytes);
	
	//read buffer
	udi_cdc_multi_read_buf(USB_DIRECTC_PORT, page_global_buffer, return_bytes);

	start_page_address = image_requested_address;
	end_page_address = image_requested_address + return_bytes - 1;
	
	return;
}

#endif



#endif


/*
* Module: dp_get_data_block_address
* 		purpose: This function sets current_block_address to the first relative address of the requested 
*       data block within the data file.
* Return value:
* 		None.
*/
void dp_get_data_block_address(DPUCHAR requested_var_ID)
{
    DPUINT var_idx;
    DPULONG image_index;
    DPUINT num_vars;
    DPUCHAR variable_ID;	
    
    /* If the current data block ID is the same as the requested one, there is no need to caluclate its starting address.
    *  it is already calculated.
    */
    
    if (current_var_ID != requested_var_ID)
    {
        current_block_address=0U;
        current_var_ID=Header_ID;
        if (requested_var_ID != Header_ID)
        {
            /*The lookup table is at the end of the header*/
            image_index = dp_get_header_bytes((HEADER_SIZE_OFFSET),1U);
            image_size = dp_get_header_bytes(IMAGE_SIZE_OFFSET,4U);
            
            
            /* The last byte in the header is the number of data blocks in the dat file */
            num_vars = (DPUINT) dp_get_header_bytes(image_index-1U,1U);
            
            for (var_idx=0U;var_idx<num_vars;var_idx++)
            {
                variable_ID = (DPUCHAR) dp_get_header_bytes(image_index+BTYES_PER_TABLE_RECORD*var_idx,1U);
                if (variable_ID == requested_var_ID)
                {
                    current_block_address = dp_get_header_bytes(image_index+BTYES_PER_TABLE_RECORD*var_idx+1U,4U);
                    current_var_ID = variable_ID;
                    break;
                }
            }
        }
    }
    return;
}

/*
* Module: dp_get_data_block_element_address
* 		purpose: This function return unsigned char pointer of the byte containing bit_index
*       within the data file.
* Return value:
* 		DPUCHAR *: unsigned character pointer to the byte containing bit_index.
*/
DPUCHAR * dp_get_data_block_element_address(DPULONG bit_index)
{
    DPULONG image_requested_address;
    /* Calculating the relative address of the data block needed within the image */
    image_requested_address = current_block_address + bit_index / 8U;
    
    #ifdef USE_PAGING
    /* If the data is within the page, adjust the pointer to point to the particular element requested */
    /* For first load, start_page_address = end_page_address = 0u */
    if (
    (start_page_address != end_page_address) &&
    (image_requested_address >= start_page_address) && (image_requested_address <= end_page_address) &&
    ((image_requested_address + MIN_VALID_BYTES_IN_PAGE) <= end_page_address)    //vllt (start_page_address + MIN_VALID_BYTES_IN_PAGE) <= image_requested_address)
    )
    {
        page_address_offset = image_requested_address - start_page_address;
        return_bytes = end_page_address - image_requested_address + 1u;
    }
    /* Otherwise, call dp_get_page_data which would fill the page with a new data block */
    else 
    {
        dp_get_page_data(image_requested_address);
        page_address_offset = 0u;
    }
    return &page_global_buffer[page_address_offset];
    #else
    return_bytes=image_size - image_requested_address;
    /*
    Misra - C 2004 deviation:
    image_buffer is a pointer that is being treated as an array.  
    Refer to DirectC user guide for more information.
    */
    return &image_buffer[image_requested_address];
    #endif
}


/*
* Module: dp_get_bytes
* 		purpose: This function is designed to return all the requested bytes specified.  Maximum is 4.
*		DPINT * var_ID, an integer variable contains an identifier specifying which 
* 		block address to return.
*		DPULONG byte_index: The relative address location of the first byte in the specified data block.
*		DPULONG requested_bytes: The number of requested bytes.
* Return value: 
* 		DPULONG:  The combined value of all the requested bytes.
* Restrictions:
*		requested_bytes cannot exceed 4 bytes since DPULONG can only hold 4 bytes.
*
*/
DPULONG dp_get_bytes(DPUCHAR var_ID,DPULONG byte_index,DPUCHAR bytes_requested)
{
    DPULONG ret = 0U;
    DPUCHAR i;
    DPUCHAR j;
    j=0U;
    
    while (bytes_requested)
    {
        page_buffer_ptr = dp_get_data(var_ID,byte_index*8U);
        /* If Data block does not exist, need to exit */
        if (return_bytes == 0u)
        {
            break;
        }
        if (return_bytes > (DPULONG) bytes_requested )
        {      
            return_bytes = (DPULONG) bytes_requested;
        }
        for (i=0u; i < (DPUCHAR) return_bytes;i++)
        {
            /*
            Misra - C 2004 deviation:
            page_buffer_ptr is a pointer that is being treated as an array.  
            Refer to DirectC user guide for more information.
            */
            ret |= ((DPULONG) (page_buffer_ptr[i])) << ( j++ * 8U); 
        }
        byte_index += return_bytes;
        bytes_requested = bytes_requested - (DPUCHAR) return_bytes;
    }
    return ret;
}

/*
* Module: dp_get_header_bytes
* 		purpose: This function is designed to return all the requested bytes specified from the header section.
*      Maximum is 4.
*		DPINT * var_ID, an integer variable contains an identifier specifying which 
* 		block address to return.
*		DPULONG byte_index: The relative address location of the first byte in the specified data block.
*		DPULONG requested_bytes: The number of requested bytes.
* Return value: 
* 		DPULONG:  The combined value of all the requested bytes.
* Restrictions:
*		requested_bytes cannot exceed 4 bytes since DPULONG can only hold 4 bytes.
*
*/
DPULONG dp_get_header_bytes(DPULONG byte_index,DPUCHAR bytes_requested)
{
    DPULONG ret = 0U;
    DPUCHAR i;
    DPUCHAR j=0u;
    
    while (bytes_requested)
    {
        page_buffer_ptr = dp_get_header_data(byte_index*8U);
        /* If Data block does not exist, need to exit */
        if (return_bytes == 0U)
        {
            break;
        }
        if (return_bytes > (DPULONG) bytes_requested )
        {
            return_bytes = (DPULONG) bytes_requested;
        }
        for (i=0u; i < (DPUCHAR)return_bytes; i++)
        {
            /*
            Misra - C 2004 deviation:
            page_buffer_ptr is a pointer that is being treated as an array.  
            Refer to DirectC user guide for more information.
            */
            ret |= (((DPULONG) page_buffer_ptr[i])) << ( j++ * 8U);            
        }
        byte_index += return_bytes;
        bytes_requested = bytes_requested - (DPUCHAR) return_bytes;
    }
    return ret;
}

