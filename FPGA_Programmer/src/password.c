/*
 * password.c
 *
 * Created: 03.05.2014 16:41:35
 *  Author: Yaro
 */ 

#include "password.h"

void waitUntilUsbPasswordReceived(void){
	char password[] = USB_PASSWORD;
	
	char receivedString[] = USB_PASSWORD;	//just to have same size of the array
	
	uint8_t passwordLength = strlen(USB_PASSWORD);
	
	memset(receivedString, 'q', passwordLength);	//clear receivedString
	
	while(strcmp(receivedString, password) != 0){
		memcpy(receivedString, &receivedString[1], passwordLength - 1);
		receivedString[passwordLength-1] = udi_cdc_multi_getc(USB_PC_INTERACTIONS_PORT);
	}
}