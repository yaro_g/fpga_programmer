/*
 * SDCard.h
 *
 * Created: 07.05.2014 21:57:58
 *  Author: Yaro
 */ 


#ifndef SDCARD_H_
#define SDCARD_H_

#include "compiler.h"
#include "sd_mmc_spi.h"
#include "debug.h"

#define SD_CARD_CLOCK_RATE 12000000UL

void initSDCardPeripherals(void);


#endif /* SDCARD_H_ */