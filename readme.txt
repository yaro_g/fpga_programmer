#define ENABLE_CODE_SPACE_OPTIMIZATION
ausschalten => Viele Pr�fungen fallen weg, daf�r weniger Code size


Action codes:										Funktion 
#define DP_DEVICE_INFO_ACTION_CODE              1   ja
#define DP_READ_IDCODE_ACTION_CODE              2   ja
#define DP_ERASE_ACTION_CODE                    3	ja
#define DP_PROGRAM_WITHOUT_VERIFY_ACTION_CODE   4	ja  
#define DP_PROGRAM_ACTION_CODE                  5	ja
#define DP_VERIFY_ACTION_CODE                   6	ja
#define DP_ENC_DATA_AUTHENTICATION_ACTION_CODE  7	nein
#define DP_ERASE_ARRAY_ACTION_CODE              8	ja
#define DP_PROGRAM_ARRAY_ACTION_CODE            9	ja
#define DP_VERIFY_ARRAY_ACTION_CODE             10	ja
#define DP_ERASE_FROM_ACTION_CODE               11	vielleicht mit entsprechendem .dat File
#define DP_PROGRAM_FROM_ACTION_CODE             12	vielleicht mit entsprechendem .dat File
#define DP_VERIFY_FROM_ACTION_CODE              13	vielleicht mit entsprechendem .dat File
#define DP_ERASE_SECURITY_ACTION_CODE           14	nein
#define DP_PROGRAM_SECURITY_ACTION_CODE         15	nein
#define DP_PROGRAM_NVM_ACTION_CODE              16	nein
#define DP_VERIFY_NVM_ACTION_CODE               17	nein
#define DP_VERIFY_DEVICE_INFO_CODE              18	ja
#define DP_READ_USERCODE_ACTION_CODE            19	ja
#define DP_PROGRAM_NVM_ACTIVE_ARRAY_CODE        20	nein
#define DP_VERIFY_NVM_ACTIVE_ARRAY_CODE         21	nein
#define DP_IS_CORE_CONFIGURED_ACTION_CODE       22	ja