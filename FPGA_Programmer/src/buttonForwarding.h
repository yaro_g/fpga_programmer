/*
 * buttonForwarding.h
 *
 * Created: 24.03.2014 19:31:37
 *  Author: Yaro
 */ 


#ifndef BUTTONFORWARDING_H_
#define BUTTONFORWARDING_H_

#include <asf.h>

void initButtonForwarding(void);
void deInitButtonForwarding(void);
inline void forwardButtons(void);

ISR(PORTA_INT0_vect);
ISR(PORTC_INT0_vect);


inline void forwardButtons(void){
	if(PORTA.IN & PIN2_bm){
		PORTC.OUTSET = PIN5_bm;
	}else{
		PORTC.OUTCLR = PIN5_bm;
	}
	
	if(PORTC.IN & PIN3_bm){
		PORTC.OUTSET = PIN2_bm;
	}else{
		PORTC.OUTCLR = PIN2_bm;
	}
}

#endif /* BUTTONFORWARDING_H_ */