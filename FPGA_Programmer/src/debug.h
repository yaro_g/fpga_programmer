/*
 * debug.h
 *
 * Created: 08.05.2014 22:06:40
 *  Author: Yaro
 */ 


#ifndef DEBUG_H_
#define DEBUG_H_

#include <asf.h>
#include <stdio.h>
#include <string.h>
#include "dpalg.h"
#include "flash_string.h"
#include "DirectC_PC_Interaction.h"
#include "PCInteractions.h"
#include "dataLinker.h"
#include "my_spi.h"
#include "uart.h"
#include "clockOutput.h"
#include "buttonForwarding.h"
#include "password.h"

#include "dpuser.h"
#include "dpcom.h"
#include "LCPS_Port_Functions.h"

#include "udi_cdc.h"

#define USE_DEBUG TRUE

uint32_t debug;

#define PRINT_DEBUG_BUF_SIZE 100
char printDebugBuffer[PRINT_DEBUG_BUF_SIZE];

void print_debug(const char* format, ...);


#endif /* DEBUG_H_ */