/*
 * temp.cpp
 *
 *  Created on: 08.11.2013
 *      Author: Yaro
 */

#include <asf.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "dpalg.h"
#include "flash_string.h"
#include "DirectC_PC_Interaction.h"
#include "PCInteractions.h"
#include "dataLinker.h"
#include "my_spi.h"
#include "uart.h"
#include "clockOutput.h"
#include "buttonForwarding.h"
#include "password.h"
#include "SDCard.h"
#include "ff.h"		/* Declarations of FatFs API */

#include "dpuser.h"
#include "dpcom.h"
#include "LCPS_Port_Functions.h"

#include "udi_cdc.h"

#include "debug.h"

static volatile bool my_flag_autorize_cdc_transfer[2] = { false, false };
bool my_callback_cdc_enable(uint8_t port) {
	my_flag_autorize_cdc_transfer[port] = true;
	return true;
}
void my_callback_cdc_disable(uint8_t port) {
	my_flag_autorize_cdc_transfer[port] = false;
}




FATFS FatFs;		/* FatFs work area needed for each volume */
FIL Fil;			/* File object needed for each open file */
FILINFO FilInfo;
char fileInput[1200];


int main(void) {
	debug = 10000;
	
	board_init();
	sysclk_init();
	
	irq_initialize_vectors();
	cpu_irq_enable();

	udc_start();
	rtc_init();
	
	initButtonForwarding();
	enableClockOutputOnPC4();
	
	sysclk_enable_peripheral_clock(&TCC0);
	TCC0.CTRLA = TC_CLKSEL_DIV1_gc;
	
/*	
	//sdCard usage example 
	//(Yaros SoC Seminararbeit)
	initSDCardPeripherals();

	uint16_t result;
	result = f_mount(0, &FatFs);		
	
	uint8_t err = sd_mmc_check(0);
//  	if (err != 0){
//  		print_debug("\n\nError Number: %i\n", (uint32_t)err);
//  	}

	result = f_stat("muster2", &FilInfo);
	//print_debug("result stat %d\n\r", result);

	result = f_open(&Fil, "muster2", FA_READ);
	//print_debug("result open %d\n\r", result);

	uint16_t fileSize = FilInfo.fsize;
	uint16_t wantToRead = 18;

	uint16_t haveRead;
	
	initSPIMaster(2);
	do {
		result = f_read (&Fil, fileInput, wantToRead, &haveRead);
		fileInput[18] = 0;
		
		char *digitPos = fileInput;
		uint8_t i;
		uint8_t data = strtoul(digitPos, &digitPos, 10);
		blockingBufferTransferSPI(&data, 1);
			
		for (i = 0; i < 3; i++){
			uint8_t data = strtoul(digitPos, &digitPos, 16);
			blockingBufferTransferSPI(&data, 1);
		}
			
		uint16_t delay = strtol(digitPos, &digitPos, 10);
		if(delay != 0){
			delay_ms(delay);
		}
	} while(wantToRead == haveRead);
	deInitSPIMaster();
	
	f_close(&Fil);
	result = f_mount(0, NULL);
*/

	while(!my_flag_autorize_cdc_transfer[0]);
	udi_cdc_multi_getc(USB_PC_INTERACTIONS_PORT); //wait for call


	while (1) {
		if (my_flag_autorize_cdc_transfer[0]) {
			waitUntilUsbPasswordReceived();
			switch(udi_cdc_multi_getc(USB_PC_INTERACTIONS_PORT)) {
				case Programmer_Mode_FPGA_PROGRAMMER:
				{
					handleProgrammerMode();
					break;
				}
				case Programmer_Mode_DATA_LINKER_SPI:
				{
					deInitButtonForwarding();
					initSPIMaster(2);  //deSelectSPISlaveFPGA is in debug state!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					//selectSPISlaveFPGA();
					
					handleDataLinkerSPI();
					
					//deSelectSPISlaveFPGA();
					deInitSPIMaster();
					initButtonForwarding();
					break;
				}
				case Programmer_Mode_DATA_LINKER_UART:
				{
					initUART(115200);
					
					handleDataLinkerUART();
									
					deInitUART();
					break;
				}
				case Programmer_Mode_INFO:
				{
					udi_cdc_multi_putc(USB_DIRECTC_PORT, USB_DEVICE_MAJOR_VERSION);
					udi_cdc_multi_putc(USB_DIRECTC_PORT, USB_DEVICE_MINOR_VERSION);
					break;
				}
			}
 		}
	}
}

//echo for different ports
// while(1){
// 	for (uint8_t port = 0; port < UDI_CDC_PORT_NB; port++){
// 		if (my_flag_autorize_cdc_transfer[port] && udi_cdc_multi_get_nb_received_data (port)) {
// 			c = udi_cdc_multi_getc (port);
// 			udi_cdc_multi_putc (port, c+port);
// 		}
// 	}
// }

//speedtest:
/*
 Init:
 sysclk_enable_peripheral_clock(&TCC0);
 TCC0.CTRLA = TC_CLKSEL_DIV1_gc;

 Task:
 udi_cdc_getc();
 rtc_set_time (0);
 TCC0.CNT = 0;

 for (uint16_t i = 0; i < 5; i++){
 udi_cdc_read_buf (buf, 100);
 }

 uint32_t time2 = (uint32_t)TCC0.CNT;

 uint32_t time1 = rtc_get_time();

 udi_cdc_write_buf(buf, sprintf(buf, "RTC:%lu  Timer%lu", time1,time2));
 */


