/*
 * flash_string.h
 *
 * Created: 10.10.2013 14:51:41
 *  Author: gevorkov
 */ 


#ifndef FLASH_STRING_H_
#define FLASH_STRING_H_

#include <avr/pgmspace.h>
#include <string.h>


#define FLASH_STRING_BUFFER_SIZE 1  //have to change if used!!!!!!!!!!  not more than 255

extern const PROGMEM char tooLongStringErrorMessage[];



char flashStringBuffer[FLASH_STRING_BUFFER_SIZE];

//#define FLASH_STRING(str) 


#define FLASH_STRING(str) getStringFromFlash(PSTR(str), strlen(str))   //have to change FLASH_STRING_BUFFER_SIZE if used!!!!!!!!!!

char* getStringFromFlash(PGM_P str, uint8_t strLength);

#endif /* PROGMEM_STRING_H_ */