/*
 * buttonForwarding.c
 *
 * Created: 24.03.2014 19:31:28
 *  Author: Yaro
 */ 

#include "buttonForwarding.h"

void initButtonForwarding(void){
	PORTA.DIRCLR = PIN2_bm;
	PORTC.DIRCLR = PIN3_bm;
	
	PORTA.PIN2CTRL = PORT_OPC_PULLDOWN_gc | PORT_ISC_BOTHEDGES_gc;
	PORTC.PIN3CTRL = PORT_OPC_PULLDOWN_gc | PORT_ISC_BOTHEDGES_gc;
	
	forwardButtons();
	
	PORTC.DIRSET = PIN5_bm | PIN2_bm;
	
	PORTA.INT0MASK = PIN2_bm;
	PORTC.INT0MASK = PIN3_bm;
	
	PORTA.INTCTRL = PORT_INT0LVL_MED_gc;
	PORTC.INTCTRL = PORT_INT0LVL_MED_gc;
}

void deInitButtonForwarding(void){
	PORTC.DIRCLR = PIN5_bm | PIN2_bm;
}

ISR(PORTA_INT0_vect){
	forwardButtons();
}

ISR(PORTC_INT0_vect){
	forwardButtons();
}